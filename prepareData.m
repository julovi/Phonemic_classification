% This script prepares an m-file to be used for the phonation
% Classification
% Julian Villegas, University of Aizu, 2022.

% Praat settings

clear all

tier = 1;
label = '*'; % use '*' to match all existing labels
srcDir = '/Users/julian/Downloads/00_Chopped_Files_WordsOnly/';
srcDir = [srcDir filesep];
dstDir = srcDir;

wavFiles = dir([srcDir '*.wav']);
tgsFiles = dir([srcDir '*.TextGrid']);

numberOfFiles = length(wavFiles);
if length(tgsFiles) < numberOfFiles
    numberOfFiles = length(tgsFiles);
end

theData = cell(numberOfFiles,2);

for i=1:numberOfFiles
    nm = wavFiles(i).name;
    [x,osr] = audioread([srcDir nm]);
    sr = 16000;
    x = resample(x,sr,osr);
    % center and remove DC
    x = x - mean(x);
    % Normalize amplitude to be 0dB FS
    x = x/max(abs(x));
    [segs,labs] = ReadPraatTier([srcDir nm(1:end-3) 'TextGrid'],tier);
%     if ~isempty(labs)
%         % find vocalic parts
%           labRegions = floor(1000*segs(~strcmpi(labs,''),:));
%     else
%         disp(['Warning: Didn''t find the labs for file ' nm '.']);
%     end
    theData{i,1} = nm;
    theData{i,2}.x = x;
    theData{i,2}.sr = sr;
    theData{i,2}.segs = segs;
    theData{i,2}.labs = labs;
end
save toClassify.mat theData

